import logo from './logo.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Congrats 🤝 !<br />
          You successfully ran the project using<br />
          <code>npm start</code>.
        </p>
        <p>
          Use <code>npm run build</code> for production
        </p>
      </header>
    </div>
  );
}

export default App;
